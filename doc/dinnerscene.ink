
//Dining room
The casserole smells roasty and comforting - like a favorite food from my childhood.
This emboldens me to try Catherine's food. I can't wait to break the texture between my teeth.
Then, Catherine stabs the casserole with a knife. It seems to screech, and to ooze something dark into the oven tray.
Is that blood?
The casserole is more raw meat than sautéd beef and vegetables. I can see the fat, pure white, coming out of the tissue.
Catherine: "Here you go."
...
Catherine: "Try it. Cut it like lasagna."
I don't still know what this casserole is made of, but I poke it with a fork. It still makes that screeching noise. My appetite is gone.
Charles: "So, [the protagonist's name]. How's your book going?"
"I've written four pages already, as a matter of fact. It's going very well. You know, I feel like I'm making great progress here."
Catherine: "That's wonderful."
Charles: "Craig, how did you find out about [the protagonist's name]?"
Craig: "Through the internet. At the school library, you know. And by the way,"
He leans towards me and lowers his voice.
Craig: "[the protagonist's name], could I talk to you after dinner?" // This is available when you talk with Craig before the dinner.
*   "Sure. I can lend you an ear or two."
    Craig: "Thanks, man."
    He relaxes into his chair, but I find I can't do the same. Charles is peering at me over the cups and bowls.
    Craig gets up to turn the arrows back on the grandfather clock. He does this right before it can reach eight o'clock.
-> novelquestiondinner
*   "Sorry. I still gotta reach my word quota for the day."
    Craig: "Aw, man. Well, it benefits me, so I won't stop you."
    That comment has Charles' eyebrows raise several inches.
-> novelquestiondinner

==novelquestiondinner==
Catherine: "I'm curious, [the protagonist's name]. What is your book about, exactly?"

* "It's about a knight, a monster, and a princess."
//+1 ending1
Catherine: "Like a fairytale?"
Caroline: "You don't seem like the type."
"I guess you could call it a bit of a dark fairytale, but fairytales are already quite dark if you look at the right versions."
Craig: "But even if fairytales are dark, in an aesthetic sense, don't they tend to comfort the reader?"
I tilt my head, a little disturbed by Craig's interruption.
"How so?"
Craig: "The reader identifies with the heroes. And the heroes are always made to feel righteous and virtuous by the end. It's reassuring."
I shrug, feeling vaguely insulted. Is he trying to say my stories are just easy-reading? What's his point?
"In that sense, I suppose you're right."
He beams at this victory over me, then goes to rewind the arrows again.
-> originquestiondinner

* "It's about a boy."
//+1 ending2
"He's a bit of a misfit, and the story is how that conflict between him and the society he's in comes to a climax. Or maybe it'll resolve in a gentle way. Who knows where the story might take me?"
I laugh sheepishly. I don't feel like these people would respect such a whimsical work process.
Charles: "A misfit, huh?
He looks at Craig, who flees the eye contact.
Charles: "If you go for the gentle resolution ending, it might be a good influence on your readers."
"I don't personally feel novelists should approach their readers as teachers. Rather as equals. But I'll take that into account."
Charles: "The point of art is to produce a studied effect on the person who experiences it, right? Who cares if you have to exert control over them to accomplish it? That isn't your concern."
I try not to gawk at him or voice my disagreement. Still, I wonder how ethical a stance that is for a scientist.
->originquestiondinner

* "I'd rather not talk about that."
//+1 ending3
I don't want to bring the subject of adultery up in front of a family I don't know. What if they have painful history with that topic? What if they interrogate my history?
"It's nothing too outrageous, but still, it's a serious subject. An adult one too."
Charles: "If Craig can handle it, can't we handle it too?"
I open my mouth, but fail to find words. He's got me.
"I know Craig a little better, and the story comes from a personal place. Give me time, and I'm sure I'll be able to talk to everyone about it."
Everyone accepts my excuse with a smile, but Craig is scowling at the clock.
->originquestiondinner

==originquestiondinner==
Charles: "I'm curious about you, [the protagonist's name]. Where are you from, originally?"

* "I don't remember."
// +1 ending1
Charles: "You don't remember? How can you not remember?"
"Sorry, it's the truth. The agency I was adopted from was quite loose with its paperwork. My file turned out to belong to someone else when I was eight. Ever since, I've had no idea about my birthplace, or my bio-parents."
Caroline: "Woah. These agencies really treat children like meat."
The piece of raw flesh floating in my mouth feels much more disgusting, and I struggle to swallow.
"I believe it was an honest mistake. But now, writing is my clearest path to forging my identity."
Charles: "That's good. Don't inherit your identity, make it."
"I intend to."
Catherine: "Do you remember things about your birthplace?"
"A few scattered images."
-> postoriginquestion

* "Irvine, California."
// +1 ending2
Catherine: "Ooh! So you're from the Blue State. That's lovely."
"Yeah, it's got it's ups and downs. It means my sister was able to go to film school right there in her hometown."
->postoriginquestion

* "Seoul, South Korea."
// +1 ending3
Charles: "That's a very long trip for a writing retreat."
"Well, you know. You've got to gorge yourself on new experiences to be a writer."
This seems like a poor choice of words, given the unappetizing spectacle on my plate.
Catherine: "And have... people made you feel welcome here?"
I dodge her implication and smile.
"Everyone's been very friendly."
It's not even untrue, but being stared at by these avid toothy grins, I feel distinctly like I'm being cast in the role of the Other.
->postoriginquestion

==postoriginquestion==
Catherine: "Do you miss it?"
"I do, but not for the reasons you might think. I just haven't gotten out much these past years, working as a writer. I can't wait to be at a place in my life where I can stop and take walks, and feel the salty breeze again."
Charles: "That may take longer than you think. It's rough, entering adult life. Professional life. You're never quite as free as you used to be."
The attic starts rattling again.
->friendquestion

==friendquestion==
Charles: "You'd never seen Craig before today, right? Is he what you expected?"
"Not exactly! But when I think about it, it gets easier and easier to link up his writing with what he looks like."
Craig turns red.
Craig: "What do you mean?"
"I mean..."
Oh dear, I talked myself into a corner.
"The way you wrote, it's like you had to externalise the alienation you felt. Your fashion expresses that in a way."
He nods quietly, and I think I spot a twitchy little smile on his lowered face. Catherine turns her head towards me slowly, barely blinking.
Catherine: "If Craig surprised you, what kind of people are you usually friends with?"

* "I like humble, diligent people."
// +1 ending1
Craig snickers.
Craig: "[the protagonist's name] likes that in girls, too."
I scowl at him. Those conversations were private.
"Yes, I suppose that's true."
Charles: "So you prefer women who are a little more traditional? A little meek, a little deferent?"
"Well, uh. I-"
Charles: "There's no need to be embarrassed. I like that type of woman too."
Catherine whispers a protestation, too quiet for me to tell the words apart. Charles squeezes her hand. It looks affectionate at first, but I see white fingerprints on her hand when he lets go.
->Guest_bedroom_night

* "People who are conscious of their flaws, and seek self-improvement."
// +1 ending2
Caroline: "That's a good way to be. I don't always have the patience to recognize people's efforts. I get annoyed and can only see what they're doing wrong."
Charles: "I'd like to think I can recognize people's efforts, but when it comes to people who wallow in their failures, don't give them second chances."
Craig wriggles in his seat and goes to rewind the clock again.
"Some people need more than two chances, and I think that's fine too. But I don't want to surround myself with people who aren't moving forward."
Charles: "Yes. Those are the people that slow you down."
Craig drops into his chair, clanging the feet against the tile floor. In his hands, he twirls his knife.
Craig: "Can we drop the topic?"
"Ah... yes, of course."
Caroline casts a sideglance at Craig. Charles is outright glowering at him.
->Guest_bedroom_night

* "Frank people, who you can be frank to in return."
// +1 ending3
Caroline: "I think I see what you mean. Between friends, it's important to be kind, but if politeness and truth end up in a fight, let the truth win."
"I just can't stand it when you give someone necessary feedback, and they run away weeping because they can't face having to see themselves differently, or having to change something about their behavior."
Caroline: "If you can't be honest with your friends..."
"You can't be honest with anyone."
She smiles at me behind a curtain of curly hair, which she tosses back to tear a piece of stringy pink meat with her teeth.
->Guest_bedroom_night


== Guest_bedroom_night ==
That night, the strangeness of that dinner sparks an idea. A new fictional obstacle that I have to implement at once in my novel. While I'm racing at a pace of 20 words per second, someone knocks at the door.
//Either Craig or Caroline comes into the room.
-> DONE