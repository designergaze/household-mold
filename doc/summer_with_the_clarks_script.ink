Day 0: 

I am writing this journal during a trip lasting a couple weeks. Not exactly a trip, but more like a retreat. The retreat to an American suburb by a Missourian creek. I need to keep writing to get my creative juice going without any distraction. I am halfway finished with the first draft, but I am like a month behind the deadline. I shouldn’t procrastinate or waste away in bed. Trying to combat my own insomnia by doing nothing was a very bad idea. 
Speaking of which, let’s see this letter a fan of mine sent me.
“Dear [protagonist name],
Thank you responding to all of mails I have written to you. And I got great news: my parents have allowed you to come over to my house and stay for few weeks to work on your novel…. You deserve better company through this month. I hope socializing with new people will help solving your writer’s block. Anyway, the address of my home is….”
Through Airbnb, I rented a room in a house where one of my fans live. We exchanged letters with one another. His letters were written on a legal pad, long-hanged, with a ball-point pen. His frenetic style looks like he’s excited to talk with his favorite writer. He believes regular correspondence is the best way to practice his writing skill. I remember him telling me he prefers handwriting because he finds it more natural than working with typewriters or computers. He said that writing with Wi-Fi on distracts him too much.
Distraction certainly has a toxic effect on creativity, but stepping away from writing has a worse one still. Even better – his house happens to be haunted, which could give me some cool ideas for my novel. I won’t be surprised if it’s built on an Indian ground burial.
Day 1:

Castleton, Missouri. A suburban house in the middle of the Paper Street. Everything looks quiet in the middle of this neighborhood. I press the doorbell.
Ding dong.
The boy's voice: coming!
The boy opens the door.
The boy: Hello. Are you [protagonist's name]?
Yes. And you must be Cornelius Craig Clark, right?
Craig: Yes I am. And holy shit - it's an honor to meet you. I am your biggest fan. Please, just call me Craig.
Craig. My first biggest fan. Big enough for him to provide me a place to stay for a few weeks He takes my suitcase inside the house as I enter.
Craig: Mom. Dad. [protagonist's name] is here.
Craig's mother: Hello. It's very nice to meet you.

/*
When the story bottlenecks from this decision, Craig gets angry that his mother called him Cornelius no matter what, even though in this branch it does not happen. I recommend you either add it, or add an if/then command of some kind. - Dan Cakeshard
*/

*   "It's nice to meet you too."
    Catherine: My name is Catherine. And this is my husband, Charles."
    
    Charles: It's very nice to meet you. I have heard so many things about you from my son. Craig, could you guide him to his room.
    Craig: Sure.

    I follow Craig upstairs. 

    -> Guest_bedroom

*   "You must be Craig's mother."
    Catherine: Yes I am. The name's Catherine. And this is my husband, Charles.
    
    Charles: It's very nice to meet you. I have heard so much things about you from my son. 
    Catherine: Cornelius, could you guide him to his room.
    Craig: It's Craig, mom! 
    Catherine: Just take him upstairs.

    I follow Craig upstair. 
    
    -> Guest_bedroom

    
== Guest_bedroom ==
    Your mother calls you Cornelius.
    
    Craig: Just ignore it. Mom always calls me Cornelius. Even though I fucking hated it. 
    
    The guest room looks cozy. Very much reminds me of my own bedroom.

    Craig: This is your bedroom.

    Do you have Wi-Fi in your house?

    Craig: Actually no, my parents were too paranoid to get an internet connection. They were worried that someone would hack into our stuffs and get all of our information. Let me know if you need anything.

    Craig's left. Now what's next?

*   Get out of the house.
    I guess I need to take a breath of fresh air before the work.
    -> Entrance
-> DONE
*   Work on your novel.
    Alright. Time to work on some stuff.
    -> novel_writing_day_one
-> DONE
*   Hang around the house.
    This seems like a cool house. Let me look around.
    I heard a noise from the hallway.
    I look up, and the door to the attic is shaking. 
    The girl: Can I help you?
    Oh.
    The door is shaking again.
    The girl: It happens. It's probably the wind.
    You must be Craig's sister.
    Caroline: Yes. My name is Caroline. It's very nice to meet you.
    It's very nice to meet you too.
    I am heading downstairs. 
    -> Living_room
-> DONE

== Entrance ==
....?
Wait. What's happening? Why aren't my legs moving? 
.....
I change my mind. I'll just look around the house then.
Girl's voice: Hello.
Um, hello?
The girl comes down stairs.
The girl: You must be that author my brother is talking about.
Yes, I am. You can call me [protagonist's name]. And it is very nice to meet you.
The girl: What are you doing in front of the door?
I..... was trying to take a walk and breathe the fresh air. I mean, there are a lot of trees in Missouri, and it seems like a good idea to go outside. But I changed my mind.
The girl looks so strange.
Anyway, what's your name?
Caroline: Caroline.
Well, Caroline, your family have a nice house.
Caroline: Mom and dad thought the house would be nice to work on their experiment. 
Experiment?
Caroline: They work at home. That's why my dad's research team come to our house for work. 
So the noise from upstairs is part of the experiment?
Caroline: You mean the noise from the attic? That's probably pigeons on the window. 
Oh. But how do you know?
Caroline: It happens often. That's what dad told me. But I advise you not to go in there.
Why not?
Caroline: I am not even allow to go in. Not even my mom or Cornelius.
You mean Craig?
Caroline: Craig is his middle name, not first.
But he insisted everyone call him Craig.
Caroline: Dad likes corny names. My mom doesn't even know where the hell he got that name. 
(heh)
Caroline: The dinner is casserole. It will be ready in 30 minutes. You could join us for tonight.
Sweet. I am starving. I never had casserole before.
Caroline: Mom thought it would be your favorite.
My favorite? I don't know about that.
.....
Can we sit down somewhere?
Caroline: Dad's working in the living room. Maybe in your bedroom?
Sure.
// Bedroom
Caroline: Cornelius told me you write short stories. And you're working on your first novel. What it's about?
Without giving too much away, it's about a family and a haunted house.
Caroline: Is it a horror novel?
You could say so. It's a ghost story, anyway. You like scary stories?
Caroline: I'm indifferent to them. Some of them genuinely scare me, but some of them are stupid. But I do like Richard Bachman books. I am not sure you would call his works horror or not.
You mean Stephen King?
Caroline: I am talking about Richard Bachman, not Stephen King.
But Richard Bachmann is Stephen King.
Caroline: What do you mean?
Stephen King sometime write novels under a pen name Richard Bachman.
Caroline: Are you making this up, because I've really never heard of this.
I am not making it up. But what's your favorite of his works?
Caroline: Rage and Roadwork. I like stories that give us a different perspective on our innate nature.
Rage is about the school shooter, right?
Caroline: I couldn't imagine someone who would be this evil. And I am surprised that people like this character exist in real-life. That's why I am very fascinated by Bachman's work. They helped me understand people around me.
But you know they are fictional?
Caroline: Fictions are often truer than facts.
....!
Hold on.
Caroline: Huh? 
I grabbed my legal pad and write some ideas that just came to my head. And Caroline is looking at me very awkwardly.
Then I heard a knock at the door.
Yes?
Craig opens it.
Craig: Caroline, I told you not to bother [the protagonist's name].
Caroline: Cornelius, we were just having a conversation.
Craig: I told you not to call me Cornelius.
Caroline: Mom told me to call you Cornelius. Now get out of his room.
Caroline leaves the room.
Hey don't treat your sister like that.
Craig:......
What's with you and your sister?
Craig: Um. Sorry for making you busy. Can I talk with you for a minute.
Wait, you don't want your sister to interrupt my work, and now you want to have a conversation with me?
Craig: It's gonna be short.
I shut the door in case if this gets too personal for both of us.
Craig: The truth is..... she's not my sister.
Who?
Craig: Caroline.
What do you mean Caroline is not your sister?
Craig: She's not my dad's daughter. Mom had an affair with someone from dad's research team. I think.
You think your mom cheated on your dad?
Craig: I don't know exactly. But this really scared me.
Why are you telling me about this?
Craig: There's no one I could share with. Then I came across your stories.
I know, I know. That's how you discovered me, and volunteered to provide me a place to work on my novel.
Craig: I mean, I wanted to talk with you directly about this rather than through letters. You know more about affairs more than anyone does, right?
That's..... complicated. But you know what, I don't think your mom had an affair. It might be a different thing.
Craig: How so?
I don't know about your mom, but I don't think she would hold that secret since her daughter's birth.
Craig: Could I talk to you right after dinner?
Sure
// Right after the dinner, you will be talking with Craig again. I think he's obsessing you.
-> dinner
-> DONE
== Living_room ==
Charles is in the living room. Doing his work, I guess.
Charles: Ah, [protagonist's name]. You need anything?
I am just looking around the house.
Charles: Around the house? It's not a big deal.
But it's a big beautiful house. There gotta be something you'd like to show. Even if it's mundane.
Charles: Well since you asked for it, I could give you a tour.
At least I didn't mention about the rumor of the house being haunted.
Charles: Well, of course, this is the living room. And on the left is the kitchen
// We're at the kitchen
Catherine: Hey, boys. I have been making some lemonade. Would you try some, Charles?
Charles: Ah, yes. My favorite drink from my wife. Thank you very much, Cathy.
Catherine: I'm also making casserole for dinner tonight. I thought [the protagonist's name] would like it.
(I never had a casserole before.)
By the way, Charles. I keep hearing noises from the attic.
Charles: Oh that?
And your daughter says it's just the wind.
Charles: It could be the wind.
Does it happen all the time?
Charles: I think. But here's the thing. As a host of this house, I would advise you not to go either in the attic or the basement.
The basement? You have a basement as well?
Charles: Yes and we keep many fragile or toxic materials down there. They are mostly related to my works. Not even Craig and Catherine are allowed to enter in.
Including Caroline?
Charles: Caroline?
Your daughter?
Charles: Oh yes. Including Caroline. And also, the attic is full of molds and fungi. The wind spread the spores so they could get into your eyes and lungs in a matter of seconds. I am allergic to them, and so are my wife and children.
Catherine: I am very sensitive toward fungi and mold.
Charles: And just recently, we've called someone to clean them up.
Has it always been like that since you moved in?
Charles: That's a long story. Would you like to have a seat on a sofa? 
Sure. I wonder, what is it you're working on, exactly? 
Charles: I could show you some of it. But only non-confidential stuff.
// We returned back to the living room.
Charles: More than half of my life, I've been living in Providence, Rhodes Island. There, I studied a niche, esoteric subject that I've never managed to adequately explain to anyone, so it's not worth to do it easily. And someone from an institution in Boston contacted me for advice in their experiment, and they eventually hired me full-time as a research professor. There, I met my wife Catherine and raised my children together. Sorry for being too vague to but this is very sensitive matter, the institution wanted to discontinue the experiment. I begged them for me to continue the experiment, so they decided to take it to Missouri and that's how my family and I ended up moving there.
So the basement is where your experiment is kept?
Charles: You could say so. In fact, Catherine is one of my researchers in this. This is very personal for her. My..... my research partner also followed us to Missouri, so he could work with us. And we hired some people from the local division to help us as well.
How's the experiment going?
Charles: We found a volunteer. 
You don't mind if I use some of this in my novel?
Charles: As long as all names stay fictional. And even if you are our guest, I can't show you around the basement.
Catherine: Hey boys. Dinner is ready.
Charles: I will get Craig and Caroline.
-> dinner
== novel_writing_day_one ==
I remember having a recurring dream about me being a creature of some kind.

A kind of creature that exists in legends or in folklore.

Living in this dream world brought me a new feeling. Like, I was completely free.

I could go wherever I wanted, but whenever I woke up, my freedom was gone.

Now I am looking for that freedom by writing my dreams down as stories. All in short sentences and few words. It's very honest and approachable when I write that way. A literary minimalism. Now, where was I?

The hero is stuck in this haunted house in the middle of suburban neighborhood. with this nice-looking family. The family wants the hero to stay because.....
The hero is a creature of some kind, like the one in my dream. They resent this creature because of..... political reasons perhaps?

Political.....

Maybe he could be the knight in a shining armor - except he's a monster. The monster who's also the knight in a shining armor. He's come out to save a princess from the evil castle. That's right. I am doing great. I get where this novel is going.

Then I heard the knocks.

Who is it?
Girl's voice: Are you the writer guy?
I open the door. And there is a girl.
Girl's voice: Hi. Sorry to bother you. You must be the writer guy Cornelius is talking about.
You mean Craig.
Caroline: His first name is Cornelius, and my name is Caroline.
Nice to meet you Caroline. I am [the protagonist's name].
Caroline: Nice to meet you too.
So, how's you and Craig?
Caroline: Ususally Cornelius pretends that I don't exist.
You wanted to get noticed by your brother?
Caroline: Not necessarily. Why would I need an attention when he makes a center of attentions for himself?
What do you mean?
Caroline: I don't want to tell this to you, but please don't tell Cornelius about this.
Okay.
Caroline: About a year ago, he and dad had an argument together.
Over what?
(They are fighting over Catherine)
Was it always like that before?
Caroline: Cornelius has been acting strange since we moved to this house. And I was like six when we moved here.
Where did you all lived before coming to Missouri?
Caroline: Boston. Boston, Massachusett. Remember how I mentioned about the experiment dad is conducting for a long time? He set this house as his and mom's work place.
(So they are working at home?)
Then I heard the knocks on the door.
Catherine: It's me!
I open the door and Catherine is carrying glasses of lemonade.
Catherine: Sorry to bother you, but would you like some lemonade?
Sure. Thank you very much!
Catherine: What are you doing at the guest's room, Caroline?
Oh don't worry, ma'am. She's not bothering me, and I've made some good progress with my works. We were just talking.
Caroline: No problem here, mom.
(And I have some questions for Catherine)
/*
I've been hearing a lot of about you from Caroline.

*   How's between you and Craig.

*   You don't look so much of a researcher.

*   What made you come to Missouri

-----All similar response----
.....
*/
Catherine: By the way, dinnner is will be soon.
//Right after dinner, you will be talking with Criag.
-> dinner
-> DONE
== dinner ==
//Dining room
The casserole smells roasty and comforting - like a favorite food from my childhood.
This made me more confident about trying Catherine's food. I couldn't wait to put this crispy texture into my mouth.
Then Catherine stabs the casserole with the knife. And it makes screeching sound and bleeds.
(Um..... is this blood oozing out of the food?)
The piece of casserole looks like a raw meat pie rather than sautéd beefs and vegetables. I could see the fat coming out in between the meats.
Catherine: Here you go.
.....
Catherine: Try it. Cut them like a lasagna.
I don't still know what this casserole is made of. I poke it with a fork and it still makes this screeching noise.
Charles: So, [the protagonist's name]. How's your book is going on.
I wrote a couple of pages. It's going very well. I have made some great progress.
Charles: Craig, how did you find out about him?
Craig: Through internet. At the school's library, you know. And by the way [the protagonist's name], could I talk to you after dinner? // This is available when you talk with Craig before the dinner.
*   Yes
-> yes_talk_with_Craig
-> DONE
*   No
-> no_talk_with_Craig
-> DONE
Charles: I am curious about you, [the protagonist's name]. Where are you from, originally?
/*
- Seoul, South Korea (you're an outsider)
If you picked "Get out" before, there is MORE likely to get the THIRD ending.
- Irvine, California (you're from Blue State)
If you picked "Hang around the house", there is MORE likely to get the SECOND ending.
- Zagreb, Croatia (you clarify that your family immigrated to the USA)
If you picked "Work on your novel" before, there is MORE likely to get the FIRST ending.
*/
// And the rest is improvisation.
-> DONE
== yes_talk_with_Craig ==
Sure. I can lend you an ear or two.
-> DONE
== no_talk_with_Craig ==
Sorry. I still gotta.
-> DONE
== Guest_bedroom_night ==
I continue writing the novel. The freaky dinner from before inspired me to write a scene in the novel.
//Either Craig or Caroline comes into the room.
-> DONE