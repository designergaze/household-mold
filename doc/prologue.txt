- Prologue 
-- Set up your character. 
-- Craig asks for your help.

(This will be in a different scene.)

Creative writing class. You got yourself a prompt that says: "Channel
your inner monster self. How does your monster looks like, what does
your monster do, and what's your monster's every day routine?"

What is your given name?

What is your surname?

What pronoun do you go by? (Subject, Object, Possessive)

You thought of yourself as a monster living in your bedroom's
closet. You write down the description of your inner monster on the
college ruled paper.

Then the boy sitting next to you whisphers to you.

"Could you do me a favor?"

Carter Craig Cernbourg. Age 17. Male. He prefers everyone to go by his
middle name. Son of an alleged mad scientist.

You are surprised how the boy you never had any interest with suddenly
came up to you for help.
