//craig's room
At four o'clock, I go to Craig's room as instructed. He's sitting on his bed, with a notebook and pen in his hands and a smirk on his face. I want to smash it off.
Somehow, the smell is even worse than before. The mix of sweat and dirty rotting dishes is overpowering. How did his parents let him get to this stage?
I want to leave the door open, in the hope that any of the others might come to my rescue, but of course, he notices.
Craig: "Close the door. This is a private conversation. I think you'll want it to be private too, once you hear what I've got to ask."
* "Think about what you're doing, Craig."
Craig: "Like you thought so hard before rifling through my stuff?"
Craig: "I'll tell you what, we can go downstairs and think on that with everyone else."
Craig: "As a family." //craig smile
My face feels numb. I shut the door and step forward.
->interrogationbegins
* I close it without a fuss.
->interrogationbegins

==interrogationbegins==
Craig gets up from his seat and gestures me to the bed he just vacated. I don't really want to sit there, but I'd also rather not drag this out. He readies his pen over a blank page, and stares at me like a blood sniffing shark.
Craig: "Good. Let's talk about you."
Craig: "You always did tell me that writing comes from necessity, right?"
Craig: "You had the experiences you had, and your stories respond to that."
What is he getting at?
Craig: "So if you wouldn't mind,
//if you wrote the knight_monster_princess story
tell me what exactly made you want to write fairytales. Wish fulfillment. Fantasies. All that stuff."
I stutter.
"I guess I... never felt like much of a hero in my real life."
He chuckles, under his breath at first, but then he throws his head back and howls laughing. I wish Caroline, Catherine, ANYONE, would come.
Craig: "Yeah!" he answers at last, breathlessly. "I guess you aren't much of a hero. I mean look at you. Look at what you've been doing, buddy. Doesn't seem very heroic on paper, does it?"
He trails off, and points to something on the floor.
Craig: "Hey, look."
He takes a dusty child's telescope from a box under the bed, as I cringe away from him. Then he puts the telescope into my hands, and raises it between our two faces. I see his features, blurred through the lense.
Craig: "This is you. This is what you do, hoping people like me blow up in a big mighty spectacle."
Then, he turns the telescope around so he's the one looking at me.
Craig: "Don't worry though. I think I can make you a hero. You'll do fine."
->interrogationcontinues

//if you wrote the about_a_boy story
tell me. What kind of person were you in school? Were you the creep who always seemed one bad day away from bringing a gun to class?"
Craig: "Or were you just the nosy bitch who'd rather treat others like puzzleboxes than fix their own life up?"
"I don't think of you as a puzzlebox, Craig."
Craig: "Bullshit!"
Craig: "If that were true, you would've just talked to me instead of lurking through my books."
Craig: "Talked to me, like I'm talking to you now."
Craig: "You might feel like a victim, but I'm treating you as a person, more than you ever did to me."
->interrogationcontinues

//if you wrote an_adult_book
could you give me your manuscript?"
I try to get up to head to the guestroom, but he catches my wrist, gripping hard.
Craig: "No, never mind. Your notebook will do. And don't try anything. I know you keep it on you."
My hands are too shaky to handle the book, and I drop it into his hands. He passes his tongue between his teeth as he flies through it.
Craig: "Oh man. This is basically a confessional. Were you gonna confess on my behalf too?"
"No. No, not at all."
Craig: "Then, one wonders what kind of research you were doing here, in a teenage boy's room. I guess that's another Hail Mary for you, huh?"
If he repeats these insinuations to his parents, he could get me into a lot of trouble.
->interrogationcontinues

==interrogationcontinues==
"Listen, I'm sorry for what I did. I shouldn't have been lurking like that. It's only that I get curious sometimes."
Craig: "Oh. You were curious. Right."
"You learn a lot about people from their environment."
Craig: "You can learn a lot about people from the way they fuck, too. That doesn't mean you should be looking through every keyhole."
"What can I do to make amends to you?"
Craig: "Amends. God, I've really got you terrified, huh. Let me think."
He wanders to his desk, tapping a rhythm on the wooden surface.
Craig: "Answer a couple more questions for me."
Craig: "How many relationships have you had? I want to know if my guess was right."

* "I've never been in a relationship."
He cackles and I feel like throwing myself out the window. What does this kid know? What right does he have to judge me?
Craig: "I could tell. You're too passive by half. How many times have you spied on girls in the bathroom though? I bet that was more frequent than 'never'."
I don't dignify the accusation with an answer.
->interrogationresolves

* "I've had a few. I kept getting dumped."
Craig: "Right, right. Because you were a creep."
"No. On two different occasions, I cheated."
Craig: "Like I said."
"It's not like that. There was a time when the girl I was with cheated on me."
Craig: "So she was a creep too. That doesn't make you less of a creep."
Who the hell does this guy think he is?
->interrogationresolves

* "I've had a few. I always dumped the other person."
Craig: "Oh? Now that I didn't expect. Why?"
"I guess I didn't feel up to a relationship. None of my partners were bad people, but it was exhausting to keep up with them, to maintain interest, to give them the time they needed."
Craig: "I see." He writes something down on his notepad. "So you're a coward."
That accusation rings a familiar bell.
"Maybe."
Craig: "Definitely. You can't be bothered actually giving to someone else. You'd rather take what you need from them when they're not looking."
Craig: "Even if that means you never have a meaningful relationship in your life."
->interrogationresolves

===interrogationresolves===
* "You might be right about me."
//counts towards friend_ending
"I know I haven't treated people right. I've tried to change before, but I guess I haven't tried hard enough."
Craig: "I just think you haven't been the center of attention enough."
Craig: "You know, people talk about being the center of attention like this indicator of vanity. But I think it's just one way to exist for others, instead of just existing for yourself."
Craig: "That'll be your therapy. You're gonna exist for my benefit, just a little while."
I recoil, and hurry towards the door.
"We'll talk some other time. I need some air."
I tell Caroline and Catherine that I intend to leave a few days early, praying they don't tell Craig about it. My sleepless nights at home seem much more welcoming now than anything I've encountered at the Clark house. But whatever Craig's threat entails, I'll be gone before it comes to fruition.
->DONE

* "What exactly do you know about relationships, Craig?"
//counts towards beating_ending
"Beyond what you've read in psychology textbooks and horror novels, what experience do you have exactly?"
Craig hides behind his telescope again.
Craig: "Look, man, I'm the author now. I get to make the hypocritical judgements. Or did you think that was your god given right only?"
"I think you dress like a pariah to justify that no one wants to talk to you."
Craig: "Stop, you wound me."
"You think to yourself 'They avoid me because they're so narrowminded.' But have you considered you might just be unlikable?"
The door opens, then, and Caroline is standing there with her mouth agape. For the first time since I've met her, she looks at her brother with sympathy.
Caroline: "Craig. Are you okay?"
Craig: "Don't- uh. Don't tell mom and dad what you just heard."
Caroline: "Fuck you. This guy's a weirdo and you should never have brought him here. What kind of person would I be if I just left this alone?"
She turns on her heels with one last look of disgust for me. Then I hear her stepping down the stairs.
"My trip is about to be cut short."
Craig narrows his eyes.
Craig: "Yeah, we'll see about that, won't we?"
->DONE