//investigatingattic
Caroline brings me the key to the attic during breakfast time, and keeps eyeing me until I get up and leave. She looks like she hasn't slept, and the side of her forehead that she keeps scratching is bleeding and infected. Catherine gives her an aspirine, but Charles doesn't seem to notice anything.
"Sorry, I gotta hurry up and get back to writing," I say with my bread half-finished in front of me. "Fixed myself a 7000 word quota today."
Catherine: "What a hard worker!"
"Heh, I do my best."
Caroline told me she'd keep everyone's attention until I was done upstairs. I hope she lives up to that promise.
I climb on a commode to reach the trap door, and try to slide the ladder down as quietly as possible. Easier said than done; it is extremely rusty.
When I climb onto the floorboards, I'm struck by how little the attic resembles the rest of the house. The Clarks keep a neat ship, with rustic character, green painted beams and arches. This attic is all bare concrete that seems to be run through with moisture and mold, of colors varying between green, orange, and black. Though the blinds are closed and seem to have been for a long time, the windows are covered in bird filth. The surface of the attic is also definitely less that the surface of the house. It seems part of it is sectioned off by a chipboard wall.
I don't see much that isn't covered in white sheets, but according to Caroline, this is where Charles stores all his correspondence. I lift the ladder up, close the door, and get to work.
->wheredoievenstart
==wheredoievenstart==
What to do next?
* I try to get through the chipboard wall.
I use the keys to unscrew the boards, one at a time. The first goes falling back, and I barely catch it before it can make a racket. It's dark inside that room. What's that I hear?
It sounds like...
...breathing.
My nerves are getting to me. I don't want to go in there for now. Let's take another avenue. I put the board back in place.
->wheredoievenstart
* I remove the sheets.
They're musty and damp, and I'd rather not be touching them right now, but I did tell Caroline I'd help her.
Underneath, I find a couple spiders a little too large for my liking, lodging in cardboard boxes of beakers, jars, and burners. Some old files too.
I open them, but the scientific jargon is beyond me. The titles say things like 'Work Method For Specimens C200 to C218', or 'Experiment 98-128: The Secondary Metabolism of Denebola Specimens'.
I'd guess Charles and Catherine study animals, but a lot of this seems like it could apply to vegetation too.
There are pictures of Charles with colleagues back at his old laboratory, but Catherine is never there.
Strange. I thought they'd been colleagues before moving here.
->wheredoievenstart
* I check the floor for hiding spots.
I knock at the floor here and there, trying to find hollows. I'm hoping with all the cacaphony that comes from here, no one will notice me making a little noise.
Eventually I find a spot on the floor with an unusual echo. I grip the floorboard and lift it. There's a weathered metallic box inside.
I think to myself that I ought to bring it straight to Caroline, but what if it's nothing? What if I get her hopes up, and it's just some angry poetry Craig wrote when he was eleven or something?
I pull the lid off.
It's photographs. Dozens of them, that seem to go from the eighties to the late nineties. Ordinary family pictures for the most part.
The only thing worth noting is that all of them comprise an old man whom I've never met. A grandfather, I assume.
Here he is, holding a blonde baby, or posing with the others in front of a beachside cliff. I guess he must have done something to alienate the family, if all his photos ended up here.
Before I put them back, another thing jumps to my mind.
All of these pictures seem to date back to before Caroline's birth, including pictures of Catherine pregnant. She looks as young and radiant as she is today.
Maybe the pregnancy was complicated, and put stress on the family that ended up shattering it. I shouldn't jump to conclusions, but at least I'll have something to report.
->wheredoievenstart
* I think I'm done.
->theoldmanawakes
==theoldmanawakes==
Just as I open the trap door and put the ladder down again, I hear a grunt behind me and pivot.
A surface of chipboard suddenly jars open; a door I hadn't noticed, for its lack of a handle.
The man who comes out moves in shambles. His hand on the door looks wretched and arthritic, with green under the nails. His silver hair looks like it hasn't been washed in weeks, and I'm willing to bet it's been longer than that.
I try to gather myself.
"Hello, there. Am I disturbing you?"
The old man gets closer, grumbling as he tries to produce words. I don't know what to do.
Man: "The question..."
His voice is a wheeze. He sounds like he's had a tracheotomy, but I see no marks of that.
Man: "The question is, am I disturbing you?"
He stops to take a breath then starts again. As his head lowers I see the brown spots that mark the very old.
Man: "You seem to be having... a merry old hunt in my apartments."
I can't bring myself to laugh it off. This man's skin is so pale it borders on the transparent. I can count the veins on his forehead. 
"I was only doing a favor for Caroline, that's all."
Man: "Caroline?"
He grabs my arm with his bony fingers.
Man: "You're a friend of the family, then?"
"I should hope so," I respond, involuntarily hunching my shoulders in discomfort.
Man: "Nice to meet you, friend. I'm the elder Mr. Charles Clark, and you happen to find yourself in my place of work."
"I'll get out of your way. Very sorry."
Charles Christopher Clark: "No, don't be ridiculous. Come in. Let me show you what I've been working on."
* "I'd really rather not."
"Sorry to be rude, but Caroline is expecting me."
Charles Christopher Clark: "Oh, Caroline this, Caroline that. Haven't they thrown the cuckoo from the nest, yet?"
My eyes widen.
"Excuse me?"
->chris_reveals
* "Well, I suppose."
I follow him into the sectioned off zone. It seems much of the old chemical equipment has been repurposed here.
Something is distilling on a shelf in the corner, and a red light illuminates a work table and a microscope. A metal plate there contains something organic looking.
Charles Christopher Clark: "This mutated specimen replicates the insides of the model organism, rather than just the exterior. When I said it could be used as a food source, they dismissed me."
I look closer, and whatever creature lies on the metal plate, it has little lungs and little bowels, and a bloated cardovascular system.
Charles Christopher Clark: "My but we've come a long way since Catherine. By the way, how's little Cornelius?"
My mouth is dry. "I suppose he's fine. Ex... excuse me."
->chris_reveals
==chris_reveals==
Charles Christopher Clark: "Come now, boy!" he barks. "What did you come here for, really?"
I stop on my way to the exit, and with my back to him, I close my eyes for a moment. I just want to leave. I just want this to be over.
"Caroline feels unwell."
Charles Christopher Clark: "Unwell? Is she manifesting? Oh, I might just come down from my attic if she manifests. I want to see it."
I don't want to understand what he means.
"Not physically unwell. She feels her father doesn't care about her."
Charles Christopher Clark: "Why should he? She's my plan, not his."
My breath catches in my chest. "You... you wanted them to have her?"
Charles Christopher Clark: "Will you stop playing dumb as to what I mean, or shall I have to speak crudely?"
I hear a cry at the bottom of the ladder, and a cloud of black hair disappear through the frame of the trap door. The old man produces a weird cry, like dry heaving, which eventually become recognizable as giggling.
"Oh, this will change the experiment variables. Do report back on anything that happens. Or- or! I think I'll follow down!"
Before he can, I sneak down the ladder. I push the ladder up though he tries to leverage his strength against mine.
Charles Christopher Clark: "This is my house, boy! My house! You think you can keep me locked inside? I know the ins and outs of this place better than anyone!"
His voice is muffled as I finally managed to shut the door and lock it. There. I'll pretend like I never opened it. Nothing has changed. We should go on like before. I hope I can convince Caroline to do the same.
->DONE