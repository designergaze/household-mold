# Household Mold

![New Game's Menu Screen](readme/new-menu.png)

## Description

One of my unfinished works from the past. It used to be called 
"Summer with/at the Clarks" and made with Ren'Py. Now currently 
remastering with Godot game engine. And hopefully completing it 
soon.

Update: (12/28/22) This project is currently in dormant. But
hopefully I am reviving it soon. 

## Tools and Softwares that I used

OS: [Arch linux](https://www.archlinux.org/)/[Manjaro](https://manjaro.org/)/Windows 11

Game Engine: [Godot Engine](https://godotengine.org/) 3.2.3 (Current), [Ren'Py](https://www.renpy.org/) (Former)

Softwares (used by me): GNU Image Manipulation Program, Krita, Audacity, Emacs, & Inky

## Credits

Story and coding by Jay Whang

Co-written and character design by [Ephraim Thomson](https://courageouscotinga.itch.io/)
