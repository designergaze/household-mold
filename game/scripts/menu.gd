extends "res://scripts/screen.gd"
# scripts/menu.gd

#onready var interactive = $interactive/marginContainer/hbox/vbox/
onready var interactive_title = $interactive/marginContainer/hbox/vbox/title
onready var title_animation = $interactive/marginContainer/hbox/vbox/title/aniplayer

func _ready():
	interactive_title.text = ProjectSettings.get("application/config/name")
	title_animation.play("title_movement")
	$transition.fade_in()
	pass # Replace with function body.

"""
func _process(delta):
	if delta:
		print($transition.layer)
	pass
"""
