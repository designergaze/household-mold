extends CanvasLayer

func _ready():
	pass

func fade_in():
	$ani_player.play("fade_in")
	pass

func fade_out():
	get_parent().pause_mode = true
	$ani_player.play("fade_out")
	pass

func _on_ani_player_animation_finished(anim_name):
	if anim_name == "fade_out":
		get_parent().pause_mode = false
		get_parent().transition_to_next_scene()
	#elif anim_name == "fade_in":
	#print(layer)
	#if get_parent().pause_mode:
	#	print("Pause: ON")
	#else:
	#	print("Pause: OFF")
	pass # Replace with function body.
