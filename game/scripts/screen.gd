extends Node2D

export (PackedScene) var next_scene
#signal new_scene

func _ready():
	#emit_signal("new_scene")
	set_process(true)
	#pause_mode = true
	pass # Replace with function body.

func transition_to_next_scene():
	if (get_tree().change_scene_to(next_scene) > 0):
		print("ERROR - Scene change.")
	pass
